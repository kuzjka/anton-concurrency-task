package ua.kuzjka.anton.concurrency.file;

import java.io.File;
import java.io.IOException;

/**
 * Provides unique temporary directories.
 * Also provides an interface to remove them on-demand or after expiration time.
 */
public interface TempDirService {

    /**
     * Creates new unique temporary directory
     * @return  Directory handle
     * @throws  IOException If temporary directory cannot be created
     */
    File createTempDir() throws IOException;

    /**
     * Removes temporary directory
     * @param dir   Directory to remove
     * @return      {@code true} if removal was successful, {@code false} otherwise
     */
    boolean removeTempDir(File dir);

    /**
     * Cleans up expired temporary directories. This is assumed to be a scheduled task.
     */
    void cleanUpExpiredDirs();
}