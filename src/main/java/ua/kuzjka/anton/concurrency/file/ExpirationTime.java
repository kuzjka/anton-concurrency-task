package ua.kuzjka.anton.concurrency.file;

import java.time.temporal.TemporalUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents expiration period in days, hours or minutes
 */
public class ExpirationTime {
    private static final Pattern EXP_TIME_PATTERN =
            Pattern.compile("(\\d+)\\s*([dhm])", Pattern.CASE_INSENSITIVE);

    private int value;
    private Unit unit;

    /**
     * Create new expiration period object
     * @param value     Amount of specified time units
     * @param unit      Time unit
     */
    public ExpirationTime(int value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    /**
     * Reads expiration period from a string consisting of integer amount and a unit representation, e.g. '12h'.
     * 'd' is for days, 'h' is for hours, 'm' is for minutes.
     * @param string    String representation of a period
     * @return          Expiration time object
     */
    public static ExpirationTime fromString(String string) {
        Matcher matcher = EXP_TIME_PATTERN.matcher(string.trim());
        if (matcher.matches()) {
            int value = Integer.parseInt(matcher.group(1));
            Unit unit = Unit.fromString(matcher.group(2));
            return new ExpirationTime(value, unit);
        } else {
            throw new IllegalArgumentException("Illegal format of expiration time: " + string);
        }
    }

    /**
     * Gets amount of specified time units
     * @return  Amount of time units
     */
    public int getValue() {
        return value;
    }

    /**
     * Gets time unit
     * @return  Time unit
     */
    public Unit getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return value + " " + unit;
    }

    public enum Unit{
        DAY, HOUR, MINUTE;

        /**
         * Reads time unit from a string.
         * @param string    String representation (case insensitive)
         * @return          Time unit
         */
        public static Unit fromString(String string) {
            switch (string) {
                case "D":
                case "d":
                    return DAY;
                case "H":
                case "h":
                    return HOUR;
                case "M":
                case "m":
                    return MINUTE;
                default:
                    throw new IllegalArgumentException("Illegal string for expiration unit " + string);
            }
        }
    }
}
