package ua.kuzjka.anton.concurrency.file;

import ch.qos.logback.core.util.FileUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Temporary directory service implementation with an in-memory index.
 */
public class TempDirServiceImpl implements TempDirService {
    private static final ExpirationTime DEFAULT_EXP_TIME = new ExpirationTime(1, ExpirationTime.Unit.DAY);
    private static final Logger logger = LoggerFactory.getLogger(TempDirServiceImpl.class);

    private ExpirationTime expirationTime;
    private File storageDir;

    /**
     * Creates service instance.
     *
     * @param tmpDirPath       Path to temporary directory storage (parent temporary dir)
     * @param expirationString String representation of temporary directory expiration time
     * @throws IOException If a service cannot prepare temporary directory storage
     */
    public TempDirServiceImpl(@Value("${tmpdir.path}") String tmpDirPath,
                              @Value("${tmpdir.expiration-time}") String expirationString) throws IOException {
        if (tmpDirPath == null) {
            throw new IllegalArgumentException("Path to temporary directory is not set");
        }

        /* check or create parent directory */
        storageDir = new File(tmpDirPath);
        if (storageDir.exists() && !storageDir.isDirectory()) {
            throw new IOException("Temporary dir storage is not a directory: " + tmpDirPath);
        } else if (!storageDir.exists() && !storageDir.mkdirs()) {
            throw new IOException("Cannot create temporary storage dir: " + tmpDirPath);
        }

        /* read expiration time */
        if (expirationString == null) {
            logger.info("Expiration time is not set, falling back to default {}", DEFAULT_EXP_TIME);
            expirationTime = DEFAULT_EXP_TIME;
        } else {
            try {
                expirationTime = ExpirationTime.fromString(expirationString);
            } catch (IllegalArgumentException e) {
                logger.warn("Illegal format of expiration time {}. Falling back to default {}",
                        expirationString, DEFAULT_EXP_TIME, e);
                expirationTime = DEFAULT_EXP_TIME;
            }
        }
    }

    /**
     * Gets expiration time.
     *
     * @return Temporary directory expiration period
     */
    public ExpirationTime getExpirationTime() {
        return expirationTime;
    }

    /**
     * Sets expiration time.
     *
     * @param expirationTime Temporary directory expiration period
     */
    public void setExpirationTime(ExpirationTime expirationTime) {
        this.expirationTime = expirationTime;
    }

    Random random = new Random();
    NavigableMap<LocalDateTime, File> filemap = new TreeMap<>();

    @Override
    public File createTempDir() throws IOException {
        synchronized (this) {
            File file = File.createTempFile("abc", null, storageDir);
            file.delete();
            file.mkdir();
            filemap.put(LocalDateTime.now(), file);
            return file;
        }
    }

    @Override
    public boolean removeTempDir(File dir) {
        if (storageDir.equals(dir.getParentFile())) {

            try {
                FileUtils.deleteDirectory(dir);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public void cleanUpExpiredDirs() {

        if (expirationTime.getUnit() == ExpirationTime.Unit.MINUTE) {
            LocalDateTime date = LocalDateTime.now().minus(expirationTime.getValue(), ChronoUnit.MINUTES);
            NavigableMap<LocalDateTime, File> exp = filemap.headMap(date, true);
            for (File file : exp.values()) {
                removeTempDir(file);
            }
        }
        if (expirationTime.getUnit() == ExpirationTime.Unit.HOUR) {
            LocalDateTime date = LocalDateTime.now().minus(expirationTime.getValue(), ChronoUnit.HOURS);
            NavigableMap<LocalDateTime, File> exp = filemap.headMap(date, true);
            for (File file : exp.values()) {
                removeTempDir(file);
            }
        }
        if (expirationTime.getUnit() == ExpirationTime.Unit.DAY) {
            LocalDateTime date = LocalDateTime.now().minus(expirationTime.getValue(), ChronoUnit.DAYS);
            NavigableMap<LocalDateTime, File> exp = filemap.headMap(date, true);
            for (File file : exp.values()) {
                removeTempDir(file);
            }
        }
    }
}

