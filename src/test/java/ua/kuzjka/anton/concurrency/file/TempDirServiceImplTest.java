package ua.kuzjka.anton.concurrency.file;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class TempDirServiceImplTest {
    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    private TempDirService service;

    @Before
    public void setUp() throws IOException {
        /* create service with immediate temporary folder expiration for testing */
        service = new TempDirServiceImpl(tmpFolder.getRoot().getPath(), "0m");
    }

    @Test
    public void testCreateDirShouldExist() throws IOException {
        File dir = service.createTempDir();
        assertNotNull(dir);
        assertTrue(dir.isDirectory());
        assertTrue(dir.exists());
    }

    @Test
    public void testCreateDirTwoDifferent() throws IOException {
        File dir1 = service.createTempDir();
        File dir2 = service.createTempDir();
        assertNotNull(dir1);
        assertNotNull(dir2);
        assertNotEquals(dir1, dir2);
    }

    @Test
    public void testRemoveDir() throws IOException {
        File dir = service.createTempDir();
        assertTrue(dir.exists());
        assertTrue(service.removeTempDir(dir));
        assertFalse(dir.exists());
    }

    @Test
    public void testRemoveDirOutsidePath() throws IOException {
        /* service should not remove any directories outside storage dir */
        TemporaryFolder anotherTempFolder = new TemporaryFolder();
        anotherTempFolder.create();
        File dir = anotherTempFolder.getRoot();
        assertTrue(dir.exists());
        assertFalse(service.removeTempDir(dir));
        assertTrue(dir.exists());
    }

    @Test
    public void testCleanUp() throws IOException, InterruptedException {
        File dir1 = service.createTempDir();
        File dir2 = service.createTempDir();
        assertTrue(dir1.exists());
        assertTrue(dir2.exists());
        /* sleep to ensure expiration */
        Thread.sleep(20);
        service.cleanUpExpiredDirs();
        assertFalse(dir1.exists());
        assertFalse(dir2.exists());
    }
}
