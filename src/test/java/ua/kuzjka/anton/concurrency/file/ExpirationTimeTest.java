package ua.kuzjka.anton.concurrency.file;

import org.junit.*;

import static org.junit.Assert.*;

public class ExpirationTimeTest {

    @Test
    public void testFromStringDays() {
        ExpirationTime time = ExpirationTime.fromString("3d");
        assertNotNull(time);
        assertEquals(3, time.getValue());
        assertEquals(ExpirationTime.Unit.DAY, time.getUnit());
    }

    @Test
    public void testFromStringHoursWithSpaces() {
        ExpirationTime time = ExpirationTime.fromString("  5   h  ");
        assertNotNull(time);
        assertEquals(5, time.getValue());
        assertEquals(ExpirationTime.Unit.HOUR, time.getUnit());
    }

    @Test
    public void testFromStringMinutesUpperCase() {
        ExpirationTime time = ExpirationTime.fromString("35M");
        assertNotNull(time);
        assertEquals(35, time.getValue());
        assertEquals(ExpirationTime.Unit.MINUTE, time.getUnit());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromStringIllegal() {
        ExpirationTime time = ExpirationTime.fromString("This is totally illegal string");
    }
}
